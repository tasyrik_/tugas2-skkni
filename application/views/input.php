<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <ol class="breadcrumb">
        <i class="fa fa-arrows"></i>&nbsp;
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
  <?php

    // flashdata error atau sukses
    if (! empty($this->session->flashdata('error'))) {
      echo $this->session->flashdata('error');
    }
  ?>
    <!-- Default box -->
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">Data Peserta</h3>
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-primary btn-md btn-sm" data-toggle="modal" data-target="#mdl-input"><i class="fa fa-plus"></i>&nbsp;&nbsp;Tambah Peserta</button>
        </div>
      </div>
      <div class="box-body" style="overflow-y: auto;">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th style="text-align: center;">No.</th>
                    <th style="text-align: center;">Nama</th>
                    <th style="text-align: center;">NIK</th>
                    <th style="text-align: center;">HP</th>
                    <th style="text-align: center;">E-mail</th>
                    <th style="text-align: center;">Skema Sertifiasi</th>
                    <th style="text-align: center;">Rekomendasi</th>
                    <th style="text-align: center;">Tanggal Terbit Sertifikat</th>
                    <th style="text-align: center;">Tanggal Lahir</th>
                    <th style="text-align: center;">Organisasi</th>
                    <th style="text-align: center;">Aksi</th>
                </tr>
            </thead>
            <tbody>
              <?php $no=1; foreach($result AS $r) { ?>
              <tr>
                  <td align="center"><?php echo $no++;?></td>
                  <td align="center"><?php echo $r['nama'];?></td>
                  <td align="center"><?php echo $r['NIK'];?></td>
                  <td align="center"><?php echo $r['HP']?></td>
                  <td align="center"><?php echo $r['email'];?></td>
                  <td align="center"><?php echo $r['skema'];?></td>
                  <td align="center"><?php echo $r['rekomendasi'];?></td>
                  <td align="center"><?php echo $r['tanggal_terbit'];?></td>
                  <td align="center"><?php echo $r['tanggal_lahir'];?></td>
                  <td align="center"><?php echo $r['organisasi'];?></td>
                  <td align="center">
                      <button class="btn btn-warning btn-xs btn-edit" title="edit"
                          data-idedt="<?php echo htmlspecialchars($r['id']);?>"
                          data-nama="<?php echo htmlspecialchars($r['nama']);?>"
                          data-nik="<?php echo htmlspecialchars($r['NIK']);?>"
                          data-hp="<?php echo htmlspecialchars($r['HP'])?>"
                          data-email="<?php echo htmlspecialchars($r['email']);?>"
                          data-skema="<?php echo htmlspecialchars($r['skema']);?>"
                          data-rekomendasi="<?php echo htmlspecialchars($r['rekomendasi']);?>"
                          data-terbit="<?php echo htmlspecialchars($r['tanggal_terbit']);?>"
                          data-lahir="<?php echo htmlspecialchars($r['tanggal_lahir']);?>"
                          data-organisasi="<?php echo htmlspecialchars($r['organisasi']);?>"
                          data-toggle="modal"
                          data-target="#mdl-edit"><i class="fa fa-edit"></i>
                      </button>
                      <button class="btn btn-danger btn-xs btn-hps" title="hapus"
                          data-idhps="<?php echo htmlspecialchars($r['id']);?>"
                          data-nama="<?php echo htmlspecialchars($r['nama']);?>"
                          data-toggle="modal" data-target="#mdl-hps"><i class="fa fa-trash"></i>
                      </button>
                  </td>
              </tr>
              <?php } ?>
            </tbody>
        </table>
      </div>
      <!-- /.box-footer-->
    </div>
    <!-- /.box -->

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- Modals -->
  <!-- Modal tambah peserta -->
  <div class="modal fade modal-default" id="mdl-input">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title"><i class="fa fa-edit"></i>&nbsp;&nbsp;Masukan Data Peserta</h4>
        </div>
        <?php echo form_open('index.php/Data/input');?>
        <div class="modal-body">
          <div class="form-group row">
            <label for="#" class="col-sm-2 control-label">Nama</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="in-nama" name="in_nama" placeholder="Nama" required="required">
            </div>
          </div>
          <div class="form-group row">
            <label for="#" class="col-sm-2 control-label">NIK</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="in-nik" name="in_nik" placeholder="NIK" required="required">
            </div>
          </div>
          <div class="form-group row">
            <label for="#" class="col-sm-2 control-label">HP</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="in-hp" name="in_hp" placeholder="Nomor Handphone">
            </div>
          </div>
          <div class="form-group row">
            <label for="#" class="col-sm-2 control-label">E-Mail</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="in-mail" name="in_mail" placeholder="E-Mail">
            </div>
          </div>
          <div class="form-group row">
            <label for="#" class="col-sm-2 control-label">Skema Sertifikasi</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="in-skema" name="in_skema" placeholder="Skema Sertifikasi" required="required">
            </div>
          </div>
          <div class="form-group row">
            <label for="#" class="col-sm-2 control-label">Rekomendasi</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="in-rekomendasi" name="in_rekomendasi" placeholder="Rekomendasi">
            </div>
          </div>
          <div class="form-group row">
            <label for="#" class="col-sm-2 control-label">Tanggal Terbit</label>
            <div class="col-sm-10">
              <input type="text" class="form-control datepicker" id="in-tgl-terbit" name="in_tgl_terbit" placeholder="Tanggal Terbit">
            </div>
          </div>
          <div class="form-group row">
            <label for="#" class="col-sm-2 control-label">Tanggal Lahir</label>
            <div class="col-sm-10">
              <input type="text" class="form-control datepicker" id="in-tgl-lahir" name="in_tgl_lahir" placeholder="Tangggal Lahir" required="required">
            </div>
          </div>
          <div class="form-group row">
            <label for="#" class="col-sm-2 control-label">Organisasi</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="in-organisasi" name="in_organisasi" placeholder="Organisasi">
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
          <button type="submit" class="btn btn-success"><i class="fa fa-save"></i>&nbsp;&nbsp;Simpan</button>
        </div>
        <?php echo form_close();?>
      </div>
    </div>
  </div>

  <!-- Modal edit peserta -->
  <div class="modal fade modal-default" id="mdl-edit">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title"><i class="fa fa-group"></i>&nbsp;&nbsp;Edit Peserta</h4>
        </div>
        <?php echo form_open('index.php/Data/edit');?>
        <div class="modal-body">
            <div class="form-group row">
              <label for="#" class="col-sm-2 control-label">Nama</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" id="ed-nama" name="in_nama" placeholder="Nama" required="required">
              </div>
            </div>
            <div class="form-group row">
              <label for="#" class="col-sm-2 control-label">NIK</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" id="ed-nik" name="in_nik" placeholder="NIK" required="required">
              </div>
            </div>
            <div class="form-group row">
              <label for="#" class="col-sm-2 control-label">HP</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" id="ed-hp" name="in_hp" placeholder="Nomor Handphone">
              </div>
            </div>
            <div class="form-group row">
              <label for="#" class="col-sm-2 control-label">E-Mail</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" id="ed-mail" name="in_mail" placeholder="E-Mail">
              </div>
            </div>
            <div class="form-group row">
              <label for="#" class="col-sm-2 control-label">Skema Sertifikasi</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" id="ed-skema" name="in_skema" placeholder="Skema Sertifikasi" required="required">
              </div>
            </div>
            <div class="form-group row">
              <label for="#" class="col-sm-2 control-label">Rekomendasi</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" id="ed-rekomendasi" name="in_rekomendasi" placeholder="Rekomendasi">
              </div>
            </div>
            <div class="form-group row">
              <label for="#" class="col-sm-2 control-label">Tanggal Terbit</label>
              <div class="col-sm-10">
                <input type="text" class="form-control datepicker" id="ed-tgl-terbit" name="in_tgl_terbit" placeholder="Tanggal Terbit">
              </div>
            </div>
            <div class="form-group row">
              <label for="#" class="col-sm-2 control-label">Tanggal Lahir</label>
              <div class="col-sm-10">
                <input type="text" class="form-control datepicker" id="ed-tgl-lahir" name="in_tgl_lahir" placeholder="Tangggal Lahir" required="required">
              </div>
            </div>
            <div class="form-group row">
              <label for="#" class="col-sm-2 control-label">Organisasi</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" id="ed-organisasi" name="in_organisasi" placeholder="Organisasi">
              </div>
            </div>
        </div>
        <div class="modal-footer">
          <input type="hidden" name="ed_id" id="ed-id"/>
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
          <button type="submit" class="btn btn-warning"><i class="fa fa-save"></i>&nbsp;&nbsp;Edit</button>
        </div>
        <?php echo form_close();?>
      </div>
    </div>
  </div>

  <!-- Modal hapus peserta -->
  <div class="modal fade modal-default" id="mdl-hps">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title"><i class="fa fa-group"></i>&nbsp;&nbsp;Hapus Peserta</h4>
        </div>
        <div class="modal-body">
          <div class="form-group row">
            <div class="col-sm-12">
              Hapus peserta <label id="nama-peserta"></label> ?
            </div>
          </div>
        </div>
        <?php echo form_open('index.php/Data/hapus');?>
        <div class="modal-footer">
          <input type="hidden" id="hps_id" name="id_hps"/>
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
          <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i>&nbsp;&nbsp;Hapus</button>
        </div>
        <?php echo form_close();?>
      </div>
    </div>
  </div>
<!-- // Modals -->

<script>

    $('.btn-edit').click(function(){
        var id      = $(this).data('idedt');
        var nama    = $(this).data('nama');
        var nik     = $(this).data('nik');
        var hp      = $(this).data('hp');
        var email   = $(this).data('email');
        var skema   = $(this).data('skema');
        var rekomendasi = $(this).data('rekomendasi');
        var tgl_terbit  = $(this).data('terbit');
        var tgl_lahir   = $(this).data('lahir');
        var organisasi  = $(this).data('organisasi');

        $('#ed-id').val(id);
        $('#ed-nama').val(nama);
        $('#ed-nik').val(nik);
        $('#ed-hp').val(hp);
        $('#ed-mail').val(email);
        $('#ed-skema').val(skema);
        $('#ed-rekomendasi').val(rekomendasi);
        $('#ed-tgl-terbit').val(tgl_terbit);
        $('#ed-tgl-lahir').val(tgl_lahir);
        $('#ed-organisasi').val(organisasi);
    });

    $('.btn-hps').click(function(){
        var id   = $(this).data('idhps');
        var nama = $(this).data('nama');

        $('#hps_id').val(id);
        $('#nama-peserta').text(nama);
    });

</script>
