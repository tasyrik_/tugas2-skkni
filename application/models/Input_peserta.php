<?php
    defined('BASEPATH') or exit('Direct access script is not allowed');

    class Input_peserta extends CI_Model {

        public function simpan()
        {
            /**
             * Data untuk memasukan inputan peserta
             *
             * @var array
             */
            $data = array(
                'nama'  => $_POST['in_nama'],
                'NIK'   => $_POST['in_nik'],
                'HP'    => $_POST['in_hp'],
                'email' => $_POST['in_mail'],
                'skema' => $_POST['in_skema'],
                'rekomendasi' => $_POST['in_rekomendasi'],
                'tanggal_terbit' => $_POST['in_tgl_terbit'],
                'tanggal_lahir' => $_POST['in_tgl_lahir'],
                'organisasi' => $_POST['in_organisasi']
            );

            return $this->db->insert('data', $data);
        }

        /**
         * Fungsi mengambil data peserta dari database
         *
         * @return      array
         */
         public function get()
         {
             $sql = "SELECT * FROM data;";

             return $this->db->query($sql)->result_array();
         }

        /**
         * Fungsi mengambil data laporan dari database dengan memanggil stored procedure
         *
         * @return      array
         */
        public function get_laporan()
        {
            $sql = "CALL cetak_data_dua();";

            return $this->db->query($sql)->result_array();
        }

        /**
         * Fungsi mengedit data peserta
         *
         * @return      boolean
         */
        public function edit()
        {
            $data = array(
                'nama'  => $_POST['in_nama'],
                'NIK'   => $_POST['in_nik'],
                'HP'    => $_POST['in_hp'],
                'email' => $_POST['in_mail'],
                'skema' => $_POST['in_skema'],
                'rekomendasi' => $_POST['in_rekomendasi'],
                'tanggal_terbit' => $_POST['in_tgl_terbit'],
                'tanggal_lahir' => $_POST['in_tgl_lahir'],
                'organisasi' => $_POST['in_organisasi']
            );

            $this->db->where('id', $_POST['ed_id']);
            return $this->db->update('data', $data);
        }

        /**
         * Fungsi menghapus data peserta
         *
         * @return      boolean
         */
        public function delete()
        {
            $sql = "DELETE FROM data WHERE id = ".$_POST['id_hps'].";";

            return $this->db->query($sql);
        }

    }
